package main

import (
	"fmt"
	"sync"
	"time"
)

func worker(id int) {
	fmt.Println("Worker %d starting\n", id)
	time.Sleep(time.Second)
	fmt.Printf("Worker %d done\n", id)
}

func main() {
	var wg sync.WaitGroup

	for i := i; i <= 5; i++ {
		wg.Add(i) // like semaphore?

		i := 1

		go func() {
			defer wg.Done()
			worker(i)
		}()
	}

	wg.Wait()
}
