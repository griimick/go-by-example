package main

import "fmt"

func MapKeys[K comparable, V any](m map[K]V) []K {
	r := make([]K, 0, len(m))
	for k := range m {
		r = append(r, k)
	}
	return r
}

type List[T any] struct {
	head, tail *ListNode[T]
}

type ListNode[T any] struct {
	next *ListNode[T]
	val T
}

func (list *List[T]) Push(v T) {
	if list.tail == nil {
		list.head = &ListNode[T]{val: v}
		list.tail = list.head
	} else {
		list.tail.next = &ListNode[T]{val: v}
		list.tail = list.tail.next
	}
}

func (list *List[T]) GetAll() []T {
	 var elems []T
	 for e := list.head; e != nil; e = e.next {
	 	elems = append(elems, e.val)
	 }
	 return elems
}

func main() {
	var m = map[int]string{1: "2", 2: "4", 3: "8"}

	fmt.Println("keys:", MapKeys(m))

	_ = MapKeys[int, string](m)

	lst := List[int]{}
	lst.Push(10)
	lst.Push(20)
	lst.Push(30)
	fmt.Println("list:", lst.GetAll())
}
