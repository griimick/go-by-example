package main

import (
	"fmt"
	"time"
)

func main() {
	timer1 := time.NewTimer(2 * time.Second) // new Promise(r => setTimeout(r, 2000))
	<-timer1.C // await 
	fmt.Println("Timer1 fired")

	timer2 := time.NewTimer(time.Second)
	go func() { // async func
		<-timer2.C // await 
		fmt.Println("Timer2 fired")
	}()

	stop2 := timer2.Stop(); // clearTimeout?
	if stop2 {
		fmt.Println("Timer2 stopped")
	}

	time.Sleep(2 * time.Second) // sleep the thread
}
