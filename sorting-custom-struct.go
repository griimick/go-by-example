package main

import (
	"fmt"
	"sort"
)

type custom struct {
	age int
	class int
}

type byClassAge []custom

// sort.Inteface needs to be implemented if 
// custom sort function is required
// Len, Swap, Less

func (s byClassAge) Len() int {
	return len(s)
}

func (s byClassAge) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s byClassAge) Less(i, j int) bool {
	if s[i].class == s[j].class {
		return s[i].age < s[j].age
	}

	return s[i].class < s[j].class
}

func main() {
	people := []custom{{2, 10}, {1, 5}, {2, 5}}
	sort.Sort(byClassAge(people))
	fmt.Println(people)
}
