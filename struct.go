package main

import "fmt"

type person struct {
	name string
	age int
}

func newPerson(name string) *person {
	p := person{name: name}
	p.age = 24
	return &p
}

func main() {
	
	fmt.Println(person{"Bob", 29})
	fmt.Println(person{name: "Alice", age: 23})
	fmt.Println(person{name: "Fred"})
	fmt.Println(&person{name: "Cat"})
	fmt.Println(newPerson("Jon"))

	s := person{name: "Sean", age: 56}
	fmt.Println(s.name)

	sp := &s
	fmt.Println(sp.age)

	sp.age = 51
	fmt.Println(sp.age)
}
