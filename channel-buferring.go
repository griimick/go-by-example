package main

import "fmt"
import "time"

func main() {

	messages := make(chan string, 2)

	go func() { 
		messages <- "ping" 
		messages <- "secondping" 
	}()

	time.Sleep(time.Second);
	msg := <-messages
	fmt.Println(msg)
	msg = <-messages
	fmt.Println(msg)
}
