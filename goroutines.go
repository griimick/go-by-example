package main

import (
	"time"
	"fmt"
)

func f(from string) {
	for i := 0; i < 3; i++ {
		fmt.Println(from, ":", i)
		time.Sleep(time.Second)
	}
}

func main() {
	f("direct")
	go f("goroutine")
	go func(msg string) {
		f(msg)
	}("going")

	time.Sleep(time.Second * 10)
	fmt.Println("done")
}
